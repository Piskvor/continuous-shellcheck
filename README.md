Continuous-shellcheck
===

`./continuous-check.sh ./sc-autofix.sh --shell=bash --color=always`

Every time a given file is updated, checks it with shellcheck. This way, the code validity is checked continually, without a need for an IDE.

Usage:
---

`./continuous-check.sh /your/script/name [--continuous-quiet] [...additional arguments for shellcheck]`

The script checks on first run, and then on every write to the file. Press `Ctrl+C` to interrupt.

Running with the `--continuous-quiet` option will only output when shellcheck complains - start the checker, set activity monitoring on the terminal, and only be alerted when something is wrong.

Requirements:
---

- [shellcheck](https://github.com/koalaman/shellcheck)
- inotifywait

sc-autofix
===

`./sc-autofix.sh /some/script.sh`

Reformats the given script, and tries to automatically fix mundane issues, if possible (e.g. missing quotes). If there isn't an obvious resolution, just prints them out and exits.
