#!/bin/bash

# Autoformats (shfmt) and autofixes (shellcheck) a shell script, if possible.
# Else just prints out the checks found.

set -euo pipefail
#set -x

FILE="$(realpath "${1:-}" 2>/dev/null || true)"
if [[ "$FILE" == "" ]]; then
	TEMP_FILE="${TMP:-/tmp}/${USER}-shellcheck.last"
	FILE="$(cat "$TEMP_FILE")"
	echo "$FILE" >/dev/stderr
fi

[[ ! -e "$FILE" ]] && exit 1

DIFF_FILE="$(mktemp "${TMP:-/tmp}/scfix.XXXXX.patch")"
if [[ -f "$DIFF_FILE" ]]; then
	trap 'rm -f "$DIFF_FILE"' EXIT
fi
if [[ -x "$(command -v shfmt)" ]]; then
	(
		cd "$(dirname "$FILE")" || exit
		shfmt -i 2 -w "$(basename "$FILE")" || true
	)
fi

if (shellcheck --shell=bash --format=diff "$FILE" >"$DIFF_FILE" 2>/dev/null); then
	exit 0
else
	if [[ "$(grep -c "^Issues were detected, but none were auto-fixable. Use another format to see them.\$" <"$DIFF_FILE" || true)" -gt 0 ]]; then
		shellcheck --shell=bash --color=always "$FILE"
		exit 1
	else
		patch <"$DIFF_FILE"
	fi
fi
