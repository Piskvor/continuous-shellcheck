#!/usr/bin/env bash

set -euo pipefail
#set -x

WATCHED_FILE="$1"
shift

QUIET_ON_OK=0
OK="OK\n"
if [[ "${1:-}" == "--continuous-quiet" ]]; then
	shift
	QUIET_ON_OK=1
	OK=""
fi

INOTIFY_WAIT="$(command -v inotifywait)"
if [[ ! -x "$INOTIFY_WAIT" ]]; then
	echo "No inotifywait!"
	echo "See e.g. https://linux.die.net/man/1/inotifywait"
	exit 3
fi

CHECK_COMMAND="${CHECK_COMMAND:-}"
if [[ -z "$CHECK_COMMAND" ]]; then
	CHECK_COMMAND="$(command -v shellcheck)"
fi
if [[ ! -x "$INOTIFY_WAIT" ]]; then
	echo "No shellcheck!"
	echo "See https://github.com/koalaman/shellcheck"
	exit 4
fi

# always check at start
FILE_MODIFIED="$WATCHED_FILE"

while :; do
	if [[ "$QUIET_ON_OK" == 0 ]]; then
		echo ''
		date
		echo '---'
	fi

	if [[ "$FILE_MODIFIED" == "$WATCHED_FILE" ]]; then
		# shellcheck disable=SC2015
		# shellcheck disable=SC2059
		${CHECK_COMMAND} "$FILE_MODIFIED" "$@" && printf "$OK" || true
	fi

	FILE_MODIFIED="$(${INOTIFY_WAIT} -q -e create,attrib,close_write,delete "${WATCHED_FILE}" --format "%w" || true)"

	# do not thrash too wildly
	sleep 0.3
done
